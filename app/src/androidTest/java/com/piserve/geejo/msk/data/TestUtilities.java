package com.piserve.geejo.msk.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObservable;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.test.AndroidTestCase;

import com.piserve.geejo.msk.data.ServiceContract;
import com.piserve.geejo.msk.data.ServiceDbHelper;
import com.piserve.geejo.msk.utils.PollingCheck;

import java.util.Map;
import java.util.Set;

/**
 * Created by Droid Space on 3/30/2015.
 */
public class TestUtilities extends AndroidTestCase
{
    static ContentValues createServiceValues()
    {
        //create a new map of values where column names are the keys
        ContentValues serviceValues = new ContentValues();

        serviceValues.put(ServiceContract.ServiceEntry.COLUMN_NAME, "Electronics Engineering");
        serviceValues.put(ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME, "Jai Bharath");
        serviceValues.put(ServiceContract.ServiceEntry.COLUMN_PRICE, 20000);
        return serviceValues;
    }

    static long insertServiceValues(Context context)
    {
        //insert our test records into the database
        ServiceDbHelper dbHelper = new ServiceDbHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues testValues = TestUtilities.createServiceValues();

        long serviceRowId;
        serviceRowId = db.insert(ServiceContract.ServiceEntry.TABLE_NAME, null, testValues);

        //Verify you got your row back
        assertTrue("Error: Failure to insert the Service Values", serviceRowId != -1);
        return serviceRowId;
    }

    static void validateCursor(String error, Cursor valueCursor, ContentValues expectedValues)
    {
        assertTrue("Empty cursor returned" + error, valueCursor.moveToFirst());
        validateCurrentRecord(error, valueCursor, expectedValues);
        valueCursor.close();
    }

    static void validateCurrentRecord(String error, Cursor valueCursor, ContentValues expectedValues)
    {
        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();

        for(Map.Entry<String, Object> entry : valueSet)
        {
            String columnName = entry.getKey();
            int idx = valueCursor.getColumnIndex(columnName);
            assertFalse("Column '" +columnName + "' not found." + error, idx == -1);

            String expectedValue = entry.getValue().toString();
            assertEquals("Value '" + entry.getValue().toString() + "' did not match the expected value '"
                    + expectedValue + "'. " + error, expectedValue, valueCursor.getString(idx));

        }
    }

     /*
        The functions we provide inside of TestProvider use this utility class to test
        the ContentObserver callbacks using the PollingCheck class that we grabbed from the Android
        CTS tests.

        Note that this only tests that the onChange function is called; it does not test that the
        correct Uri is returned.
     */

    static class TestContentObserver extends ContentObserver
    {
        final HandlerThread mHT;
        boolean mContentChanged;

        static TestContentObserver getTestContentObserver()
        {
            HandlerThread ht = new HandlerThread("ContentObserverThread");
            ht.start();
            return new TestContentObserver(ht);
        }

        public TestContentObserver(HandlerThread ht)
        {
            super(new Handler(ht.getLooper()));
            mHT = ht;
        }

        // On earlier versions of Android, this onChange method is called
        @Override
        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            mContentChanged = true;
        }

        public void waitForNotificationOrFail()
        {
            // Note: The PollingCheck class is taken from the Android CTS (Compatibility Test Suite).
            // It's useful to look at the Android CTS source for ideas on how to test your Android
            // applications.  The reason that PollingCheck works is that, by default, the JUnit
            // testing framework is not running on the main Android application thread.
            new PollingCheck(5000)
            {
                @Override
                protected boolean check() {
                    return mContentChanged;
                }
            }.run();
            mHT.quit();
        }
    }

    static TestContentObserver getTestContentObserver()
    {
        return TestContentObserver.getTestContentObserver();
    }
}
