package com.piserve.geejo.msk.data;

import android.test.AndroidTestCase;

/**
 * Created by Droid Space on 3/26/2015.
 */
public class TestPractice extends AndroidTestCase
{
    //This gets run before every test
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
    }

    public void testThatDemonstratesAssertions() throws Throwable
    {
        int a = 5;
        int b = 3;
        int c = 5;
        int d = 9;

        assertEquals("X should be equal", a, c);
        assertTrue("Y should be true", d>5);
        assertFalse("Z should be false", a==d);

        if(b>10)
        {
            fail("XX should never happen");
        }
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
}
