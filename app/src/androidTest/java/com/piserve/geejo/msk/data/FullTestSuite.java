package com.piserve.geejo.msk.data;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Created by Droid Space on 3/26/2015.
 */

/*Contains code to includes all classes from the Java Test classes in its package into a suite
* of tests that JUnit will run. This allows us to add additional tests by just adding additional
* Java class files to our test directory*/
public class FullTestSuite extends TestSuite
{
    public static Test suite()
    {
        return new TestSuiteBuilder(FullTestSuite.class).includeAllPackagesUnderHere().build();
    }

    public FullTestSuite()
    {
        super();
    }
}
