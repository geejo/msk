package com.piserve.geejo.msk.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import com.piserve.geejo.msk.data.ServiceContract;
import com.piserve.geejo.msk.data.ServiceDbHelper;

import java.util.HashSet;

/**
 * Created by Droid Space on 3/26/2015.
 */
public class TestDb extends AndroidTestCase
{
    public static final String LOG_TAG = TestDb.class.getSimpleName();

    // Since we want each test to start with a clean slate
    void deleteTheDatabase() {
        mContext.deleteDatabase(ServiceDbHelper.DATABASE_NAME);
    }

    /*
        This function gets called before each test is executed to delete the database.  This makes
        sure that we always have a clean test.
     */
    public void setUp() { deleteTheDatabase();  }

    /*
        Uncomment this test once we've written the code to create the Location
        table.

        Note that this only tests that the Location table has the correct columns, since we
        give you the code for the weather table.
     */
    public void testCreateDb() throws Throwable
    {
        // build a HashSet of all of the table names we wish to look for
        // Note that there will be another table in the DB that stores the
        // Android metadata (db version information)
        final HashSet<String> tableNameHashSet = new HashSet<String>();
        tableNameHashSet.add(ServiceContract.ServiceEntry.TABLE_NAME);

        mContext.deleteDatabase(ServiceDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new ServiceDbHelper(this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        // have we created the tables we want?
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("Error: This means that the database has not been created correctly", c.moveToFirst());

        // verify that the tables have been created
        do
        {
            tableNameHashSet.remove(c.getString(0));
        } while( c.moveToNext() );

        // if this fails, it means that your database doesn't contain both the location entry
        // and weather entry tables
        assertTrue("Error: Your database was created without service entry table",
                tableNameHashSet.isEmpty());

        // now, do our tables contain the correct columns?
        c = db.rawQuery("PRAGMA table_info(" + ServiceContract.ServiceEntry.TABLE_NAME + ")", null);

        assertTrue("Error: This means that we were unable to query the database for table information.",
                c.moveToFirst());

        // Build a HashSet of all of the column names we want to look for
        final HashSet<String>   serviceColumnHashSet = new HashSet<String>();
        serviceColumnHashSet.add(ServiceContract.ServiceEntry._ID);
        serviceColumnHashSet.add(ServiceContract.ServiceEntry.COLUMN_NAME);
        serviceColumnHashSet.add(ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME);
        serviceColumnHashSet.add(ServiceContract.ServiceEntry.COLUMN_PRICE);

        int columnNameIndex = c.getColumnIndex("name");
        do
        {
            String columnName = c.getString(columnNameIndex);
            serviceColumnHashSet.remove(columnName);
        } while(c.moveToNext());

        // if this fails, it means that your database doesn't contain all of the required service
        // entry columns
        assertTrue("Error: The database doesn't contain all of the required service entry columns",
                serviceColumnHashSet.isEmpty());
        db.close();
    }

    /*
        Here is where you will build code to test that we can insert and query the
        database.  Look in TestUtilities where we can use the "createServiceValues" function.
        Also make use of the validateCurrentRecord function from within TestUtilities.
     */

    public void testServiceTable()
    {
        // First step: Get reference to writable database
        // If there's an error in those massive SQL table creation Strings,
        // errors will be thrown here when you try to get a writable database.
        ServiceDbHelper dbHelper = new ServiceDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Second Step (Service): Create service values
        ContentValues serviceValues = TestUtilities.createServiceValues();

        // Third Step (Weather): Insert ContentValues into database and get a row ID back
        long serviceRowId = db.insert(ServiceContract.ServiceEntry.TABLE_NAME, null, serviceValues);
        assertTrue(serviceRowId != -1);

        // Fourth Step: Query the database and receive a Cursor back
        // A cursor is your primary interface to the query results.
        Cursor serviceCursor = db.query
                (
                        ServiceContract.ServiceEntry.TABLE_NAME,  // Table to Query
                        null, // leaving "columns" null just returns all the columns.
                        null, // cols for "where" clause
                        null, // values for "where" clause
                        null, // columns to group by
                        null, // columns to filter by row groups
                        null  // sort order
                );

        // Move the cursor to the first valid database row and check to see if we have any rows
        assertTrue( "Error: No Records returned from service query", serviceCursor.moveToFirst() );

        // Fifth Step: Validate the location Query
        TestUtilities.validateCurrentRecord("testInsertReadDb ServiceEntry failed to validate",
                serviceCursor,
                serviceValues);

        // Move the cursor to demonstrate that there is only one record in the database
        assertFalse("Error: More than one record returned from service query",
                serviceCursor.moveToNext() );

        // Sixth Step: Close cursor and database
        serviceCursor.close();
        dbHelper.close();
    }

}
