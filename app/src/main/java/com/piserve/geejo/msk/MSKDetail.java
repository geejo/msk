package com.piserve.geejo.msk;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.TextView;

import org.w3c.dom.Text;


public class MSKDetail extends ActionBarActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mskdetail);
        if (savedInstanceState == null)
        {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mskdetail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            startActivity(new Intent(this, MSKSettings.class));
            //Set up settings Intent
            //Intent settingsIntent = new Intent(this, MSKSettings.class);
            //Start the settings Activity
            //startActivity(settingsIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class DetailFragment extends Fragment
    {
        //Shows the Activity name where the error/warning/result messages pop-up
        public static final String LOG_TAG = MSKDetail.class.getSimpleName();
        private String mServiceStr; //The string that's received from the main activity

        public DetailFragment()
        {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState)
        {
            View rootView = inflater.inflate(R.layout.fragment_mskdetail, container, false);
            TextView detailTextView = (TextView) rootView.findViewById(R.id.detailTextView);

            //The detail activity called via Intent.
            Intent detailIntent = getActivity().getIntent();

            //Find if the intent's being called and has a message passed.
            if(detailIntent!=null && detailIntent.hasExtra(Intent.EXTRA_TEXT))
            {
                //Read what message has been passed during onClick from Fragment
                mServiceStr = detailIntent.getStringExtra(Intent.EXTRA_TEXT);
                //Set the string in the textView
                detailTextView.setText(mServiceStr);
            }

            return rootView;
        }
    }
}
