package com.piserve.geejo.msk.data;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.piserve.geejo.msk.R;

/**
 * Created by Droid Space on 4/10/2015.
 */
public class ServiceAdapter extends CursorAdapter
{
    public ServiceAdapter(Context context, Cursor c, int flags)
    {
        super(context, c, flags);
    }

    private String convertCursorRowToUXFormat(Cursor cursor) {
        // get row indices for our cursor
        int idx_name = cursor.getColumnIndex(ServiceContract.ServiceEntry.COLUMN_NAME);
        int idx_organizationName = cursor.getColumnIndex(ServiceContract.ServiceEntry.COLUMN_ORGANIZATION_NAME);
        int idx_price = cursor.getColumnIndex(ServiceContract.ServiceEntry.COLUMN_PRICE);

        return cursor.getString(idx_name) + "-" + cursor.getString(idx_organizationName) +
                " - " + cursor.getInt(idx_price);
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_main, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView tv = (TextView)view;
        tv.setText(convertCursorRowToUXFormat(cursor));
    }
}
