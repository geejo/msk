package com.piserve.geejo.msk;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.piserve.geejo.msk.data.ServiceContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

import com.piserve.geejo.msk.data.ServiceContract.ServiceEntry;

/**
 * Created by Droid Space on 4/9/2015.
 */
public class FetchServicesTask extends AsyncTask<String, Void, Void>
{
    private final String LOG_TAG = FetchServicesTask.class.getSimpleName();
  //  private ArrayAdapter<String> mForecastAdapter; (Not required anymore)
    private final Context mContext;

    private boolean DEBUG = true;

//    public FetchServicesTask(Context context, ArrayAdapter<String> serviceAdapter)
//    {
//        mContext = context;
//    }

    public FetchServicesTask(Context context )
    {
        mContext = context;
    }

            //---------------------- ATTRIBUTES TO BE DISPLAYED IN UI -------------------------//

    //Filters out the required data and number of results for the UI

    // Now we have a String representing the complete forecast in JSON Format.
    // Fortunately parsing is easy:  constructor takes the JSON string and converts it
    // into an Object hierarchy for us.

    // These are the names of the JSON objects that need to be extracted.

    private String[] getServicesDatafromJson(String serviceJsonStr) throws JSONException
    {
        final String MSK_NAME = "name";
        final String MSK_PRICE = "price";
        final String MSK_ORG_NAME = "organizationName";
        final String MSK_POSTED_USER = "postedUser";
        final String MSK_PROFILE = "profile";

        int totalResults = 25;
        String resultStrs[] = new String[totalResults];

        try
        {
            //Parse the JSON for the items required
            JSONArray serviceArray = new JSONArray(serviceJsonStr);

            // Insert the new service information into the database
            Vector<ContentValues> cVVector = new Vector<ContentValues>(serviceArray.length());

            for(int i=0;i<serviceArray.length();i++)
            {
                String name, organizationName;
                // TODO: Convert price to Int
                String price;

                JSONObject serviceObject = serviceArray.getJSONObject(i);

                name =  serviceObject.getString(MSK_NAME);
                price = serviceObject.getString(MSK_PRICE);

                JSONObject postedUserObject = serviceObject.getJSONObject(MSK_POSTED_USER);
                JSONObject profileObject = postedUserObject.getJSONObject(MSK_PROFILE);

                organizationName = profileObject.getString(MSK_ORG_NAME);

                ContentValues serviceValues = new ContentValues();

                serviceValues.put(ServiceEntry.COLUMN_NAME, name);
                serviceValues.put(ServiceEntry.COLUMN_ORGANIZATION_NAME, organizationName);
                serviceValues.put(ServiceEntry.COLUMN_PRICE, price);

                cVVector.add(serviceValues);

                // resultStrs[i] = name + "-" + price + "-" + organizationName;
            }


            int inserted = 0;

            if(cVVector.size()>0)
            {
                ContentValues[] cvArray = new ContentValues[cVVector.size()];
                cVVector.toArray(cvArray);

                inserted = mContext.getContentResolver().bulkInsert(ServiceEntry.CONTENT_URI, cvArray);
            }

            Log.d(LOG_TAG, "FetchServicesTask Complete. " + inserted + " Inserted");

//            //To display the result in LOG
//            for(String s: resultStrs)
//            {
//                Log.v(LOG_TAG, "Services List Entry: " + s);
//            }
//
//            return resultStrs;
        }

        catch(JSONException e)
        {
            Log.e(LOG_TAG, e.getMessage(), e);
            e.printStackTrace();
        }
        return null;
    }

    //////////////////////////////////////NETWORKING BOILER PLATE//////////////////////////////
    @Override
    protected Void doInBackground(String... params)
    {
        HttpURLConnection urlConnection = null; //Streaming data using HTTP
        String serviceJsonStr = null; //raw JSON response
        BufferedReader reader = null; //read text from character i/p stream

        int pageNo = 1;
        int numResults = 5;

        try
        {
            //-------------------------CONNECTION--------------------//

            final String SERVICE_BASE_URL = "http://myservicekart.com/public/";
            final String SEARCH_BASE_URL = "http://myservicekart.com/public/search?";
            final String SEARCH_PARAM = "search";
            final String PAGE_PARAM = "pageno";


                /*Using a helper class UriBuilder for building and manipulating URI references*/

            Uri builtServiceUri = Uri.parse(SERVICE_BASE_URL);
            Uri builtSearchUri = Uri.parse(SEARCH_BASE_URL).buildUpon().
                    appendQueryParameter(SEARCH_PARAM, "").
                    appendQueryParameter(PAGE_PARAM, Integer.toString(pageNo)).
                    build();

            //Construct the URL for MyServiceKart query
            //Instantiate the URL object with the Target URL of the resource to request.
            URL searchUrl = new URL(builtSearchUri.toString());

            //Print out the SearchUri and see if that's what expected.
            Log.v(LOG_TAG, "Built SearchUri" +builtSearchUri.toString());

//                URL searchUrl = new URL("http://myservicekart.com/public/search?search=&pageno=1");

            //New connection is opened whenever needed.
            urlConnection = (HttpURLConnection) searchUrl.openConnection();
            //Set the type of request method to POST.
            urlConnection.setRequestMethod("GET");
            //Request the server to connect to the URL
            urlConnection.connect();

            //------------------------BUFFERING---------------------//

            //Response body is read from the stream using getInputStream
            InputStream inputStream = urlConnection.getInputStream();

            StringBuffer buffer = new StringBuffer();
            //If the stream doesn't contain data, no point in parsing
            if(inputStream==null)
            {
                return null;
            }

            //Wrap an existing reader and buffer the input
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line = null; //stores each line of data
            //Read every line till the end
            while((line = reader.readLine()) != null)
            {
                //Write data into string buffer object
                buffer.append(line + "\n");
            }

            //If there is data in StringBuffer convert it into string and store the response
            if(buffer.length()==0)
            {
                return null;
            }
            serviceJsonStr = buffer.toString();
            getServicesDatafromJson(serviceJsonStr);
            Log.v(LOG_TAG, "Services JSON String: " +serviceJsonStr);
        }

        catch(IOException e)
        {
            Log.e(LOG_TAG, "Error cannot connect to URL", e);
            return null;
        }

        catch (JSONException e)
        {
            e.printStackTrace();
        }

        finally
        {
            //If the connection is still open
            if(urlConnection!=null)
            {
                urlConnection.disconnect();
            }
            //If buffer is still being read
            if(reader!=null)
            {
                try
                {
                    reader.close();
                }
                catch (final IOException e)
                {
                    Log.e(LOG_TAG,"Error closing stream",e);
                }
            }
        }

//        try
//        {
//            //Going to return the required data for displaying in UI
//            return getServicesDatafromJson(serviceJsonStr);
//        }
//
//        catch(JSONException e)
//        {
//            Log.e(LOG_TAG, e.getMessage(), e);
//            e.printStackTrace();
//        }

        return null;

    }

//    @Override
//    protected void onPostExecute(String[] result)
//    {
//        if (result != null && mForecastAdapter != null)
//        {
//            mForecastAdapter.clear();
//            for(String dayForecastStr : result)
//            {
//                mForecastAdapter.add(dayForecastStr);
//            }
//            // New data is back from the server.
//        }
//    }
}
