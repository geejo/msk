package com.piserve.geejo.msk;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
//import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.piserve.geejo.msk.data.ServiceAdapter;
import com.piserve.geejo.msk.data.ServiceContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link } interface
 * to handle interaction events.
 * Use the {@link MSKFragment#} factory method to
 * create an instance of this fragment.
 */
public class MSKFragment extends android.support.v4.app.Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor>
{
    private ServiceAdapter mServiceAdapter;

    public static final int SERVICE_LOADER = 0;

    public MSKFragment()
    {

    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getLoaderManager().initLoader(SERVICE_LOADER, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_fragment_msk, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_refresh)
        {
            //Moving functions to a helper class, so that when Activity starts the data can be displayed
            updateServices();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        mServiceAdapter = new ServiceAdapter(getActivity(), null, 0);

        View rootView = inflater.inflate(R.layout.fragment_msk, container, false);

        ListView listView = (ListView) rootView.findViewById(R.id.listViewMainService);

        listView.setAdapter(mServiceAdapter);

        return rootView;
    }

    @Override
    public void onStart()
    {
        super.onStart();
        //Refresh called and service updated when activity starts
        updateServices();
    }

    //Calling FetchServiceTask and executing it.
    private void updateServices()
    {
        FetchServicesTask serviceTask = new FetchServicesTask(getActivity());
        serviceTask.execute();
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int i, Bundle args)
    {
        Uri serviceUri = ServiceContract.ServiceEntry.buildServiceUri(i);
        return new android.support.v4.content.CursorLoader(getActivity(), serviceUri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data)
    {
        mServiceAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader)
    {
        mServiceAdapter.swapCursor(null);
    }

}
